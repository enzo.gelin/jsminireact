import Router from './Router.js';
import './hangman.js';
import { generateTable } from './main.js';

var myVar;

function loaderPage() {
    
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("bodyGeneral").style.display = "block";
}
loaderPage();

function cachePage() {
    document.getElementById("loader").style.display = "block";
    document.getElementById("bodyGeneral").style.display = "none";
}

function loaderPageBis() {
    cachePage();
    myVar = setTimeout(showPage, 2000);
}


// Charge la vue (hash correspond a l'"ancre dans l'url")
function loadview(hash)
{
    // debugger;
    const id = 'view';
    var router = new Router();
    router.addroute("home", 'home.html', id);
    router.addroute("page1", 'page1.html', id);
    router.addroute("page2", 'page2.html', id);
    // window.addEventListener('hashchange', router.router());
    // window.addEventListener('load', router.router());
    router.router(hash)
}

//Init au debut
loadview();

// Event listener du menu generale (change de page)
document.getElementById('list').addEventListener('click' , event => {
    loaderPageBis();
    if(event.target.attributes[1].value.slice(1) == 'page1'){
        setTimeout(function(){ generateTable(); }, 1500);
    }
    loadview(event.target.attributes[1].value.slice(1));
})

// Reset pseudo inscrit au debut
function resetPseudo()
{
    sessionStorage.removeItem('person');
    document.getElementById("pseudo").innerHTML = "Pseudo : UNDEFINED";

}
document.getElementById('accountcancel').addEventListener('click', resetPseudo)


// Active link menu
var header = document.getElementById("list");
var btns = header.getElementsByClassName("nav-link");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  if (current.length > 0) { 
    current[0].className = current[0].className.replace(" active", "");
  }
  this.className += " active";
  });
}

