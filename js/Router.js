// Router js

export default class Router {
    constructor() {
        this.routes = {};
    }

    addroute(pathName, path, idview) {
        this.routes[pathName] = { path: path, idview: idview };
    }

    remove(name) {
        delete this.routes[name];
    }

    navigate(pathName) {
        this.ajaxCall(this.routes[pathName].path, this.routes[pathName].idview);
    }

    router(hash) {
        
        if(hash != undefined){
            var url = hash;
        } else {
            var url = location.hash.slice(1) || 'home';
        }
        
        // Pseudo definission (ne doit pas etre la)
        if (sessionStorage.getItem("person")) {
            document.getElementById("pseudo").innerHTML = "Pseudo : " + sessionStorage.getItem("person");
        } else {
            document.getElementById("pseudo").innerHTML = "Pseudo : UNDEFINED";
        }
        // Pseudo definission (ne doit pas etre la)
        if (url == 'page1') {
            if (sessionStorage.getItem("person")) {
                // do nothing
            } else {
                var person = prompt("Rentrer un pseudo", "UserOne");
                if (person != null) {
                    sessionStorage.setItem('person', person);
                    document.getElementById("pseudo").innerHTML = "Pseudo : " + sessionStorage.getItem("person");
                } else {
                    alert('merci de rentrer un pseudo');
                }
            }
            
        }
        
        this.navigate(url);
    }

    ajaxCall(Url, idview) {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                if (xmlhttp.status == 200) {
                    document.getElementById(idview).innerHTML = xmlhttp.responseText;

                    // Ne doit pas etre la mais cest le dessin 
                    // const hangmanPicture = document.getElementById('pendupict');
                    // hangmanPicture.style.backgroundImage =`url(../images/pendu11.png)`;
                    // hangmanPicture.style.visibility = "visible";

                }
                else if (xmlhttp.status == 400) {
                    alert('There was an error 400');
                }
                else {
                    alert('something else other than 200 was returned');
                }
            }
        };

        xmlhttp.open("GET", Url, true);
        xmlhttp.send()
    }

}