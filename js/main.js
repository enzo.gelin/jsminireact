var letters = [];
export function generateTable(letter = false) {
    const tdListener = (e) => {
        const td = e.currentTarget,
            textNode = td.childNodes[0],
            text = textNode.textContent,
            input = document.createElement("input");
        input.value = text;
        input.addEventListener("blur", inputListener);
        td.replaceChild(input, textNode);
        input.focus();
    }

    const inputListener = (e) => {
        const input = e.currentTarget,
            text = input.value,
            textNode = document.createTextNode(text);
        input.removeEventListener("blur", inputListener);
        input.parentNode.replaceChild(textNode, input);
    }
    
    document.getElementById("placeletters").innerHTML = "";
    const root = document.getElementById("placeletters");
    const table = document.createElement("table");
    const tbody = document.createElement("tbody");
    debugger;
    let word = sessionStorage.getItem("word");
    let arrayWord = word.split('');
    letters.push(letter);
    for (let i = 0; i < 1; i++) {
        const tr = document.createElement("tr");
        for (let j = 0; j < word.length; j++) {
            const td = document.createElement("td");
            td.addEventListener("click", tdListener);
            if(arrayWord[j] == letter){
                var textNode = document.createTextNode(letter);
            } else if(letters.includes(arrayWord[j]) == true) {
                var textNode = document.createTextNode(arrayWord[j]);
            } else {
                var textNode = document.createTextNode("X");
            }
            td.appendChild(textNode);
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    root.appendChild(table);
}

