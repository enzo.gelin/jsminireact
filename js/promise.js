function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min) * 1000;
}

const getWord = async function () {
    const response = await fetch("https://random-word-api.herokuapp.com/word?number=1");
    const data = await response.json();
    return data;
};

const mapping = async function () {
    const results = await Promise.all([getWord()]);
    const word = results[0];
    // document.getElementById("placeletters").innerHTML = word[0]
    sessionStorage.removeItem('word');
    sessionStorage.setItem('word', word);
    return word.map((word) => {
        return word;
    });
};

const timer = async function () {
    setTimeout(() => {}, 6000);
};

Promise.race([mapping(), timer()])
    .then(() =>  console.error("Merge ok"))
    .catch(() => console.error("Timeout"));

